
NoSpam API
==========

The XML-RPC API is designed to be a functional subset of the one
offered by `blogspam.net <http://blogspam.net>`_. Two methods are
available:

*string* **testComment** (*struct* data)
    Test the comment for spamminess. The *data* parameter is a struct
    that can have the following attributes:

    *comment*
        the text of the comment (utf-8 encoded, MANDATORY)
    *site*
        the URL of your website (unused for now)
    *agent*
        the User-Agent supplied by the user, if any
    *email*
        the email address supplied by the user, if any
    *link*
        the homepage link supplied by the user, if any
    *name*
        the name supplied by the user, if any
    
    Any other attributes will be ignored. The return value will be a
    string, formatted as following:

    OK
        comment is not spam
    SPAM:*msg*
        comment is spam, *msg* will have details
    ERROR:*msg*
        an error occurred while processing the comment


*string* **classifyComment** (*struct* data)
    Manually classify a comment as spam/non-spam. The *data* struct has
    the same attributes as the one in the testComment() call, with an
    additional mandatory attribute:

    *train*
        desired classification, either 'ok' or 'spam'

    The return value is a string, formatted as following:

    OK
        classification was successful
    ERROR:*msg*
        an error occurred while processing the comment

    For all practical purposes, the return value can be safely ignored.

