
Training and testing
====================

Before putting your NoSpam service in production, you should train
it using samples of ham/spam comments. NoSpam provides tools for
you to do so.

The scripts in the train/ directory allow for training and testing
the service using pre-existing data. It is expected that this data
will be formatted as a stream of pickled Python dictionaries, each
having the attributes required by the testComment() method, plus a
*train* attribute (as used in classifyComment()) which is required
to train on the dataset.

A simple script to generate training data from a (multisite)
Wordpress installation is provided in ``train/wpdump.py``.


