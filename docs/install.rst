
Installation
============

To run your own NoSpam service, you will need Python (>= 2.5), some
required Python modules, libevent (>= 1.4), and the crm binary from
the `CRM114 Discriminator <http://crm114.sourceforge.net>`_. Python
dependancies can be easily installed with setuptools.

On a Debian system, this is all you need to do::

    $ apt-get install python python-setuptools python-gevent crm114
    $ git clone http://git.autistici.org/nospam
    $ cd nospam ; python setup.py install

*Note:* on Debian "lenny", you will need to install libevent-dev
from the backports repository.

Alternatively, a Debian package is provided, just add the A/I
package repository::

    deb http://deb.autistici.org/debian unstable main

then you can simply install the *nospam* package.


Configuration
-------------

NoSpam reads a YAML-formatted configuration file, by default
located in /etc/nospam/config.yml (you can specify a different
configuration file by running nospamd with the --config option).
The YAML file is expected to contain a dictionary.

The following parameters are supported (specific plugins may
have their own additional configuration parameters):

port
    TCP port to listen on for requests (default 9001).
config\_dir
    Directory where to load configuration data (for example, the
    stopwords file). By default it is defined as the same directory
    holding the configuration file.
data\_dir
    Directory where to store runtime data: databases, caches, etc. This
    should be on fast persistent storage.
spam\_threshold
    A float value representing the score threshold after which a
    comment is marked as spam.
autolearn\_threshold
    If the score for a comment exceeds this threshold, the comment is
    fed back into the system as spam.


Running
-------

You can run the NoSpam daemon as follows::

    $ nospamd --config=config.yml --debug

Without --debug, the daemon will fork into the background (and log
its messages to syslog).


