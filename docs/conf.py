project = 'NoSpam'
version = '0.2.3'
language = 'en'
master_doc = 'single_page'
exclude_patterns = ['about.rst']

html_theme = 'minimal-hyde'
html_theme_path = ['/usr/share/sphinx/themes', '.']

html_show_sourcelink = False
html_show_sphinx = False
html_show_copyright = False

