
Writing new NoSpam plugins
==========================

The service is easily extensible using a plugin architecture: just
put your code in the plugins/ directory and create a subclass of
nospam.plugin\_base.BasePlugin. You can then implement any of the
following methods:

def \_\_init\_\_(self, config)
    Initialize the plugin (if needed). Only a single instance of the
    plugin object will be used in the process, so this is a good place
    for doing process-wide initialization. In case of configuration or
    other run-time errors at inizialization time, just raise any
    Exception, and the plugin will be disabled.

def testComment(self, comment)
    Test the given comment. This method should return a (score, status)
    tuple as a result. *score* must be 0 if the comment is thoght to be
    non-spam, and *status* must be the string "OK". Any non-zero score
    means that the comment is spam, and *status* is interpreted as a
    short diagnostic message.

def classifyComment(self, comment)
    The given comment was manually marked as spam/non-spam. The desired
    classification can be accessed as ``comment['train']``.

There are a few simple examples of plugins in the nospam/plugins/
directory in the source code.


