#!/usr/bin/python

from setuptools import setup, find_packages

setup(
  name="nospam",
  version="0.3",
  description="Spam classifier for NoBlogs.",
  author="ale",
  author_email="ale@incal.net",
  url="http://code.autistici.org/p/nospam",
  install_requires=["gevent", "Jinja2", "Flask", "six", "gglsbl"],
  setup_requires=[],
  zip_safe=False,
  packages=find_packages(),
  package_data={"nospam":["templates/docs/*.html", "templates/*.html"]},
  include_package_data=True,
  entry_points={
    "console_scripts": [
      "nospamd = nospam.main:main",
    ],
  },
  )

