FROM debian:bullseye-slim AS build
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        python3 python3-setuptools python3-wheel

COPY setup.* /src/
COPY nospam/ /src/nospam/
WORKDIR /src
RUN python3 setup.py bdist_wheel

FROM debian:bullseye-slim

COPY --from=build /src/dist/nospam*.whl /tmp/
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        curl crm114 python3 python3-setuptools python3-pip \
        python3-gevent python3-googleapi python3-yaml python3-bsddb3 && \
    pip3 install /tmp/nospam*.whl && \
    rm -fr /var/lib/apt/lists/* && rm -f /tmp/*.whl

COPY nospam/conf/update-surbl /usr/bin/update-surbl
COPY nospam/conf/docker-start.sh /usr/bin/nospamd

ENTRYPOINT ["/usr/bin/nospamd", "--foreground"]
