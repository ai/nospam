import os
import pickle
import logging
import shelve
import shutil
try:
    from UserDict import DictMixin
except ImportError:
    from collections import UserDict as DictMixin
try:
    from bsddb3 import db
    bsddb_available = True
except ImportError:
    bsddb_available = False


class connect_db(object):
    """Interface to a bsddb environment.

    Taken from http://code.autistici.org/svn/lens/trunk/lens/logdb.py 
    """ 
    def __init__(self, path=".", skip_db_config=False):
        # check that the environment is properly set up
        self.path = path
        if not os.path.isdir(path):
            os.makedirs(path)
        # Copy the default db config (./data/DB_CONFIG) to the path given in self.path
        db_config_dst = os.path.join(path, "DB_CONFIG")
        if not skip_db_config and not os.path.exists(db_config_dst):
            db_config_src = os.path.join(os.path.dirname(__file__),
                                         "data", "DB_CONFIG")
            shutil.copy(db_config_src, db_config_dst)
        logging.info("opening bdb database environment...")
        # Open an instance of the berkeley db
        self.env = db.DBEnv()
        self.env.set_lk_detect(db.DB_LOCK_MINWRITE)
        # Just run the recovery.
        try:
            self.env.open(path, db.DB_CREATE | db.DB_INIT_CDB | db.DB_INIT_MPOOL)
        except db.DBRunRecoveryError: #TODO: fix this
            logging.error("bdb recovery failed! run catastrophic recovery manually")
            raise
        logging.info("bdb database environment ready")

    def open(self, dbname, dup=False, create=True):
        _db = db.DB(self.env)
        flags = 0
        if create:
            flags |= db.DB_CREATE
        _db.open("%s.db" % dbname, db.DB_BTREE, flags, 0o600)
        return _db

    def close(self):
        self.env.close()


class dbshelve(DictMixin):

    def open(self, data_file):
        if os.path.isdir(data_file):
            raise  IOError('Datafile cannot be a directory')
        data_dir = os.path.dirname(data_file)
        self._env = connect_db(data_dir)
        self._db = self._env.open(os.path.basename(data_file))

    def __getitem__(self, attr):
        if not isinstance(self._env, connect_db):
            raise IOError('shelve store is not opened')
        if not self._db.exists(attr):
            raise KeyError
        try:
            value = pickle.loads(self._db.get(attr))
        except:
            value = None
        return value

    def __setitem__(self, attr, value):
        if not isinstance(self._env, connect_db):
            raise IOError('shelve store is not opened')
        if self._db.exists(attr):
            self._db.delete(attr)
        try:
            value = pickle.dumps(value)
        except:
            value = None
        self._db.put(attr, value)
        return True

    def __delitem__(self, attr):
        if not isinstance(self._env, connect_db):
            raise IOError('shelve store is not opened')
        if self._db.exists(attr):
            self._db.delete(attr)
        return True

    def close(self):
        self._db.close()
        self._env.close()

    def iteritems(self):
        try:
            cur = self._db.cursor()
            while cur.next():
                (key, rawvalue) = cur.current()
                try:
                    value = pickle.loads(rawvalue)
                except pickle.UnpicklingError:
                    value = None
                yield (key, value)
        finally:
            cur.close()

    def keys(self):
        return [key for key, _ in self.iteritems()]


def get_shelve(name, concurrent=False):
    if concurrent and bsddb_available:
        _db = dbshelve()
        _db.open(name)
        return _db
    return shelve.open(name)
