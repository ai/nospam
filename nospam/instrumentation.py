import six
import time
import resource


def _get_memory_usage():
    return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

def _get_cpu_usage():
    usage = resource.getrusage(resource.RUSAGE_SELF)
    wall_time = time.time() - _start_time
    return (usage.ru_utime / wall_time)


_start_time = time.time()
_vars = {'cpu_usage': _get_cpu_usage,
         'mem_usage': _get_memory_usage}


def set_var(key, value):
    _vars[key] = value
register_var = set_var


def incr_counter(key, amount=1):
    if key in _vars:
        _vars[key] += amount
    else:
        _vars[key] = amount


def get_metrics():
    out = []
    for key, value in six.iteritems(_vars):
        if callable(value):
            value = value()
        out.append('%s %s\n' % (key, value))
    return ''.join(out)
