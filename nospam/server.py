#!/usr/bin/python

import gevent
from gevent import pywsgi
from gevent import monkey
monkey.patch_all()

from flask import Flask, request, render_template, abort, make_response, jsonify
from nospam.flaskext.xmlrpc import XMLRPCHandler

from nospam import api
from nospam import instrumentation

app = Flask(__name__)
rpchandler = XMLRPCHandler('blogspam')
rpchandler.connect(app, '/RPC2')


@rpchandler.register
def testComment(comment):
    return app.blogspam.testComment(comment)


@rpchandler.register
def classifyComment(comment):
    return app.blogspam.classifyComment(comment)


@app.route('/', methods=('GET',))
def homepage():
    n_ham, n_spam = app.nospam.stats()
    seen_comments = n_ham + n_spam
    if seen_comments > 0:
        pct_spam = 100.0 * n_spam / seen_comments
    else:
        pct_spam = 0
    return render_template('index.html', seen_comments=seen_comments,
                           pct_spam=pct_spam, num_ham=n_ham, num_spam=n_spam)


@app.route('/docs/<page>')
def staticfile(page):
    if not page.endswith('.html'):
        page += '.html'
    try:
        return render_template('docs/' + page)
    except:
        abort(404)


@app.route('/metrics')
def metrics():
    resp = make_response(instrumentation.get_metrics())
    resp.headers['Content-Type'] = 'text/plain'
    return resp


@app.route('/api/test', methods=('POST',))
def json_api_test():
    return jsonify(app.jsonapi.test_comment(request.json))


@app.route('/api/classify', methods=('POST',))
def json_api_classify():
    return jsonify(app.jsonapi.classify_comment(request.json))


def make_server(config):
    app.nospam = api.NoSpam(config)
    app.blogspam = api.BlogSpamCompatibleAPI(app.nospam)
    app.jsonapi = api.JSONAPI(app.nospam)

    addr = ('0.0.0.0', int(config.get('port')))
    app.logger.info("serving on %s", addr)
    return pywsgi.WSGIServer(addr, app.wsgi_app)


def run_server(server):
    server.serve_forever()
