from __future__ import print_function

import os
import re
import logging
import pymysql
import collections
import sys


def _connect(url, defaults_file=None):
    m = re.search(r'^mysql://([^/]*)/(.*)$', url)
    if not m:
        raise Exception('malformed db url')
    netloc, path = m.groups()
    user_pw, host_port = None, None
    if netloc and '@' in netloc:
        user_pw, host_port = netloc.split('@', 1)
    user, pw = None, None
    if user_pw and ':' in user_pw:
        user, pw = user_pw.split(':', 1)
    if host_port and ':' in host_port:
        host, port = host_port.split(':', 1)
    else:
        host, port = host_port, 3306
    try:
        return pymysql.connect(host=host, port=int(port), user=user,
                               passwd=pw, db=path.strip('/'), charset='utf8',
                               read_defaults_file=defaults_file)
    except Exception as e:
        logging.error('could not connect to to %s: %s', url, e)
        raise


BLOG_FIELDS = ('blog_id, site_id, domain, path, registered, last_updated, '
               'public, archived, mature, spam, deleted, lang_id')
POST_FIELDS = ('id, post_author, post_date, post_date_gmt, post_content, '
               'post_title, post_excerpt, post_status, comment_status, '
	       'ping_status, post_password, post_name, to_ping, pinged, '
               'post_modified, post_modified_gmt, post_content_filtered, '
               'post_parent, guid, menu_order, post_type, post_mime_type, '
	       'comment_count')
COMMENT_FIELDS = ('comment_id, comment_post_id, comment_author, comment_author_email, '
                  'comment_author_url, comment_author_ip, comment_date, '
                  'comment_date_gmt, comment_content, comment_karma, comment_approved, comment_agent, comment_type, comment_parent, '
                  'user_id')

WPBlog = collections.namedtuple('WPBlog', BLOG_FIELDS)
WPPost = collections.namedtuple('WPPost', POST_FIELDS)
WPComment = collections.namedtuple('WPComment', COMMENT_FIELDS)


class WPDb(object):
    """Interface to the Wordpress database.

    This class supports the A/I HyperDB distributed database
    configuration, and tries to mimic its behavior.

    Define the database topology by writing connection URIs:

        mysql://user:password@host:port/dbname

    one per line, in a file named ~/.noblogs_wpdb. The file
    location can be overridden with the NOBLOGS_WPDB environemnt
    variable.
    """

    def __init__(self):
        self._db = _connect(os.getenv('DBURL'),
                            os.getenv('DEFAULTS_FILE'))

    def _cursor(self, blog_id=None):
        return self._db.cursor()

    def get_blogs(self, **args):
        """Search blogs.

        Returns a list of all blogs that satisfy the criteria specified
        in 'args'. All key=value pairs are ANDed in a WHERE clause.
        """
        c = self._cursor()
        query_values = []
        query_clauses = []
        for key, value in args.iteritems():
            query_clauses.append('%s = %%s' % key)
            query_values.append(value)
        q = 'SELECT %s FROM wp_blogs' % BLOG_FIELDS
        if args:
            q += ' WHERE %s' % ' AND '.join(query_clauses)
        res = c.execute(q, tuple(query_values))
        try:
            return map(WPBlog._make, c.fetchall())
        finally:
            c.close()

    def get_blog(self, blog_id):
        """Return metadata on a specific blog."""
        c = self._cursor()
        res = c.execute(
            'SELECT %s FROM wp_blogs WHERE blog_id=%%s' % BLOG_FIELDS,
            (blog_id,))
        return WPBlog._make(c.fetchone())

    def get_blog_option(self, blog_id, option_name, default=None):
        c = self._cursor(blog_id)
        res = c.execute(
            'SELECT option_value FROM wp_%d_options WHERE option_name=%%s' % (
               blog_id,), (option_name, ))
        value = default
        if res:
            row = c.fetchone()
            if row and row[0]:
                value = row[0] 
        return value

    def get_posts(self, blog_id, **args):
        """Search posts of a single blog.

        Returns a list of all posts for blog 'blog_id' that satisfy
        the criteria specified in 'args'. All key=value pairs are ANDed
        in a WHERE clause.
        """
        c = self._cursor(blog_id)
        query_values = []
        query_clauses = []
        for key, value in args.iteritems():
            query_clauses.append('%s = %%s' % key)
            query_values.append(value)
        q = 'SELECT %s FROM wp_%d_posts' % (POST_FIELDS, blog_id)
        if args:
            q += ' WHERE %s' % ' AND '.join(query_clauses)
        res = c.execute(q, tuple(query_values))
        try:
            return map(WPPost._make, c.fetchall())
        finally:
            c.close()

    def get_comments(self, blog_id, **args):
        c = self._cursor(blog_id)
        query_values = []
        query_clauses = []
        for key, value in args.iteritems():
            query_clauses.append('%s = %%s' % key)
            query_values.append(value)
        q = 'SELECT %s FROM wp_%d_comments' % (COMMENT_FIELDS, blog_id)
        if args:
            q += ' WHERE %s' % ' AND '.join(query_clauses)
        res = c.execute(q, tuple(query_values))
        try:
            return map(WPComment._make, c.fetchall())
        finally:
            c.close()


if __name__ == '__main__':
    logging.basicConfig()
    if len(sys.argv) < 2:
        logging.error('Not enough arguments')
        sys.exit(2)

    # Dump comments for the given blog IDs.
    wp = WPDb()
    for blog_id in sys.argv[1:]:
        comments = wp.get_comments(int(blog_id), comment_type='comment')
        for c in comments:
            print('%s' % (c,))

