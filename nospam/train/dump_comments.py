import argparse
import json
import pymysql
import sys


def _connect(dbid):
    return pymysql.connect(user='root', charset='utf8', db=f'noblogs_{dbid}', use_unicode=False,
                           unix_socket='/var/run/mariadb-noblogs/server.sock')

def _d(s, dflt=''):
    if not s:
        return dflt
    return s.decode('utf8', errors='replace')


def dump_comments(conn, blog_id, charset):
    c = conn.cursor(pymysql.cursors.DictCursor)
    # This can be used to work around double-encoding issues and other
    # problems with the database contents.
    if charset:
        c.execute("SET NAMES '%s'" % charset)
    c.execute(f"SELECT comment_author, comment_author_email, comment_author_url, comment_content, comment_agent, comment_type, comment_approved, user_id FROM wp_{blog_id}_comments")
    for row in c.fetchall():
        comment = {
            'author': _d(row['comment_author']),
            'author_email': _d(row['comment_author_email']),
            'author_url': _d(row['comment_author_url']),
            'user_agent': _d(row['comment_agent']),
            'comment': _d(row['comment_content']),
            'type': _d(row['comment_type'], 'comment'),
            'approved': _d(row['comment_approved']),
            'site': blog_id,
        }
        if row['user_id']:
            comment['user_id'] = row['user_id']
        yield comment
    c.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--charset', help='run an extra SET NAMES query')
    parser.add_argument('database_id', help='database shard ID')
    parser.add_argument('blog_ids', nargs='+')
    args = parser.parse_args()

    # Dump comments for the given blog IDs.
    conn = _connect(args.database_id)
    for blog_id in args.blog_ids:
        for comment in dump_comments(conn, blog_id, args.charset):
            print(json.dumps(comment))


if __name__ == '__main__':
    main()
