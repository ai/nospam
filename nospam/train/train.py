#!/usr/bin/python3

import argparse
import json
import sys
from xmlrpc.client import ServerProxy


def _convert_attrs(comment):
    obj = {
        'comment': comment['comment'] or '',
        'site': comment['site'],
    }
    if comment['author']:
        obj['name'] = comment['author']
    if comment['author_url']:
        obj['link'] = comment['author_url']
    if comment['author_email']:
        obj['email'] = comment['author_email']
    if comment['user_agent']:
        obj['agent'] = comment['user_agent']

    # Comments from logged-in users are ok unless explicitly marked as spam.
    if comment.get('user_id') and comment['approved'] != 'spam':
        obj['train'] = 'ok'
    elif comment['approved'] == '1':
        obj['train'] = 'ok'
    else:
        obj['train'] = 'spam'

    return obj


def read_stream(fd):
    for line in fd:
        if not line:
            break
        yield _convert_attrs(json.loads(line))


def do_train(server, stream):
    for comment in stream:
        server.classifyComment(comment)


def do_test(server, stream):
    def parse_result(s):
        if s == 'OK':
            return 'ok'
        print(s)
        return 'spam'

    err = 0
    ok = 0
    err_by_type = {'ok': 0, 'spam': 0}
    counts = {'ok': 0, 'spam': 0}
    ref_counts = {'ok': 0, 'spam': 0}
    fp_msgs = []
    for comment in stream:
        try:
            expected = comment.get('train', 'ok')
            result_msg = server.testComment(comment)
            result = parse_result(result_msg)
            ref_counts[expected] += 1
            counts[result] += 1
            if result != expected:
                err_by_type[result] += 1
                err += 1
                if expected == 'spam':
                    print('Missed comment: %s' % str(comment))
                else:
                    print('False positive: %s' % str(comment))
                    fp_msgs.append(result_msg)
            else:
                ok += 1
        except Exception as e:
            print('exception:', e, comment, file=sys.stderr)

    print('\nFalse positives:')
    for i in fp_msgs:
        print(' - %s' % i)
    print('\n%d comments tested, ok=%d, err=%d, accuracy=%g%%' % (
        (err + ok), ok, err, 100 * float(ok) / (err + ok)))
    print('expected: ham=%d, spam=%d' % (ref_counts['ok'], ref_counts['spam']))
    print('result: ham=%d, spam=%d' % (counts['ok'], counts['spam']))
    print('misclassified as: ham=%d, spam=%d' % (err_by_type['ok'], err_by_type['spam']))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', dest='server_url',
                        default='http://localhost:9001',
                        help='URL of the XMLRPC server.')
    parser.add_argument('command', choices=['train', 'test'])
    args = parser.parse_args()

    server = ServerProxy(args.server_url)
    stream = read_stream(sys.stdin)
    if args.command == 'train':
        do_train(server, stream)
    else:
        do_test(server, stream)


if __name__ == '__main__':
    main()
