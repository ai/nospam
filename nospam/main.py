#!/usr/bin/python

from nospam import server
from nospam import urls
import gevent
from gevent.signal import signal as gevent_signal
import logging
import logging.handlers
import optparse
import os
import yaml
import pwd
import signal
import sys

log = logging.getLogger(__name__)


class PIDFile(object):

    def __init__(self, path):
        self.path = path

    def acquire(self):
        if os.path.exists(self.path):
            cur_pid = int(open(self.path).read().strip())
            try:
                os.kill(cur_pid, 0)
                raise RuntimeError(
                    'another instance is already running, pid=%d' % cur_pid)
            except OSError:
                log.info('stale lock found (pid=%d)' % cur_pid)
        fd = open(self.path, 'w')
        fd.write('%d\n' % os.getpid())
        fd.close()

    def release(self):
        try:
            os.unlink(self.path)
        except:
            pass


def daemonize(pidfile, username, main_function):
    # Our own daemonizing function. We don't want to close the existing
    # file descriptors, as gevent and logging need some of them.
    if os.fork():
        return

    gevent.reinit()

    if username:
        os.setuid(pwd.getpwnam(username).pw_uid)

    try:
        pidfile.acquire()
    except Exception as e:
        log.error('could not write pidfile: %s' % str(e))
        sys.exit(1)

    def _sigterm_handler(signo):
        pidfile.release()
        log.info('terminated by signal %d' % signo)
        sys.exit(0)
    gevent_signal(signal.SIGTERM, _sigterm_handler, signal.SIGTERM)
    gevent_signal(signal.SIGINT, _sigterm_handler, signal.SIGINT)

    null_fd = open(os.devnull, 'r+')
    for stream in (sys.stdin, sys.stdout, sys.stderr):
        os.dup2(null_fd.fileno(), stream.fileno())
    os.setsid()
    try:
        log.info('starting daemon...')
        main_function()
    finally:
        pidfile.release()


def start(opts):
    # Configuration defaults go here.
    config = {'port': 9001,
              'spam_threshold': '2.0',
              'data_dir': '/var/lib/nospam',
              'config_dir': os.path.dirname(opts.config)}
    try:
        config.update(yaml.load(open(opts.config, 'r')))
    except Exception as e:
        log.error('error loading configuration file %s: %s' % (
                opts.config, str(e)))

    urls.init_urls(config)

    def _sighup_handler():
        log.info('SIGHUP received, reloading config')
        urls.init_urls(config)
    gevent_signal(signal.SIGHUP, _sighup_handler)

    server.run_server(server.make_server(config))


def main():
    parser = optparse.OptionParser()
    parser.add_option('-d', '--debug', dest='debug', action='store_true',
                      help='Print debug messages.')
    parser.add_option('--syslog', action='store_true',
                      help='Send log messages to syslog.')
    parser.add_option('--foreground', action='store_true',
                      help='Do not fork into the background.')
    parser.add_option('-c', '--config', dest='config', 
                      default='/etc/nospam/config.yml',
                      help='Configuration file (default: /etc/nospam/config.yml).')
    parser.add_option('--pidfile', dest='pidfile',
                      default='/var/run/nospam.pid',
                      help='Location of the PID lock file (default: /var/run/nospam.pid).')
    parser.add_option('--user', dest='user',
                      help='Run as this user (default: current user).')
    opts, args = parser.parse_args()
    if len(args) != 0:
        parser.error('Too many arguments')

    # Set up logging, according to command-line options.
    logger = logging.getLogger()
    if opts.debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    if opts.syslog:
        handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                 facility=3)
        handler.setFormatter(logging.Formatter(
                'nospam[%(process)d]: (%(module)s) %(levelname)s: %(message)s'))
        logger.addHandler(handler)
        logger.setLevel(logging.INFO)
    else:
        logging.basicConfig()

    if opts.foreground:
        start(opts)
    else:
        def do_start():
            start(opts)
        pidfile = PIDFile(opts.pidfile)
        daemonize(pidfile, opts.user, do_start)


if __name__ == '__main__':
    main()
