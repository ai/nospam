import os
import logging
import threading
from encodings import punycode
from nospam.plugin_base import BasePlugin

try:
    from gglsbl import SafeBrowsingList
    has_gglsbl = True
except ImportError:
    has_gglsbl = False


class GoogleSafeBrowsingPlugin(BasePlugin):
    """Tests any url found in the page text against the Google Safe
    Browsing service."""

    def __init__(self, config):
        if not has_gglsbl:
            raise Exception('Missing gglsbl dependency')
        api_key = config.get('safebrowsing_api_key')
        if not api_key:
            raise Exception('Google SafeBrowsing API key not configured')
        data_dir = config.get('data_dir', './data')
        dbpath = os.path.join(data_dir, 'nospam_gsb_v4.db')

        self._codec = punycode.Codec()

        self._sbl = SafeBrowsingList(
            api_key,
            db_path=dbpath,
            timeout=10,
        )
        self._ready = threading.Event()
        threading.Thread(target=self._sync, args=(self,), daemon=True)

    def _sync(self):
        try:
            self._sbl.update_hash_prefix_cache()
            self._ready.set()
        except Exception as e:
            logging.error('Error synchronizing SafeBrowsing cache: %s', e)

    def testComment(self, comment):
        if self._ready.is_set():
            for url in comment['_urls']:
                # ASCII-encode url according to RFC 3492
                ascii_url, length = self._codec.encode(url)
                result = self._sbl.lookup_url(ascii_url)
                if result:
                    return (self.SCORE, 'Blocked by Google SafeBrowsing: %s (%s)' % (
                        url, str(result)))

        return (0, 'OK')

    def classifyComment(self, comment):
        pass

