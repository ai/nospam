import re
from nospam.plugin_base import BasePlugin


class MultiLinksPlugin(BasePlugin):

    LINK_PATTERNS = [re.compile(r'(&lt;|<)a href=(["\']|&quot;)', re.I),
                     re.compile(r'\[url=["\']?https?:', re.I),
                     re.compile(r'(^|[ \t]+)https?:', re.I)]

    def testComment(self, comment):
        count = 0
        for pat in self.LINK_PATTERNS:
            if pat.search(comment['comment']):
                count += 1
        if count > 1:
            return (self.SCORE, 'multilinks')
        else:
            return (0, 'OK')
