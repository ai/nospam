import re
from nospam.plugin_base import BasePlugin


class NoTextPlugin(BasePlugin):

    _tag_pattern = re.compile(r'<[^>]*>')

    def testComment(self, comment):
        text = self._strip_tags(comment['comment']).strip()
        if not text:
            return (self.SCORE, "no text")
        return (0, "OK")

    def _strip_tags(self, text):
        return self._tag_pattern.sub('', text)
