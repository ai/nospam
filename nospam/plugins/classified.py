import os
from datetime import date
from math import exp
from nospam.dbshelve import get_shelve
from nospam.plugin_base import BasePlugin
from nospam import urls


class ClassifiedPlugin(BasePlugin):
    """Tests for domains already present in spammy comments.

    Should trigger spam alert for any domain seen in spam comments.
    """

    TAU = 7.0

    def __init__(self, config):
        data_dir = config.get('data_dir', './data')
        self._dict = get_shelve(os.path.join(data_dir, 'domains_dict'),
                                config.get('concurrent', False))
        self.max_new_links = config.get('classified_max_new_links', 100)
        #http.HTTPServer.register_url('/classified', self._dump_stats)

    def testComment(self, comment):
        domains = urls.get_domains(comment['_urls'])
        for host in domains:
            shost = str(host)
            if not shost:
                continue
            try:
                (numLinks, lastSeen) = self._dict[shost]
            except KeyError:
                continue
            today = date.today()
            dampFactor = (
                numLinks * exp((today.toordinal() - lastSeen.toordinal()) / self.TAU))
            if dampFactor > self.max_new_links:
                return (self.SCORE, "more than %d new links from domain %s" % (
                        self.max_new_links, host))
        return (0, "OK")

    def classifyComment(self, comment):
        domains = urls.get_domains(comment['_urls'])
        for host in domains:
            shost = str(host)
            if comment['train'] == 'spam':
                try:
                    self._dict[shost] = (self._dict[shost][0] + 1, date.today())
                except KeyError:
                    self._dict[shost] = (1, date.today())
            else:
                self._dict[shost] = (0, date.today())

    def _dump_stats(self, req):
        out = []
        for key in self._dict:
            out.append((self._dict[key][0], key))
        out.sort(reverse=True)
        data = ''.join(['%d %s\n' % x for x in out[:100]])
        req.add_output_header('Content-Type', 'text/plain')
        req.add_output_header('Content-Length', str(len(data)))
        req.send_reply(200, 'OK', data)
