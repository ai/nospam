import re
from nospam.plugin_base import BasePlugin


class NoFollowPlugin(BasePlugin):

    # Wordpress comments will start with <a href="#comment-NNN"
    # rel="nofollow"> when you reply to an existing comment...
    # Ignore that.
    _nofollow_patterns = (
        re.compile(r'''<a href=["'][^#"][^"']+["']\s+[^>]*rel=["']?nofollow'''),
        re.compile(r'''<a\s+rel=["']?nofollow[^>]+href='''),
    )

    def testComment(self, comment):
        for rx in self._nofollow_patterns:
            if rx.search(comment['comment']):
                return (self.SCORE, 'nofollow')
        return (0, 'OK')
