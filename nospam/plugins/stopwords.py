import logging
import os
import re
from nospam.plugin_base import BasePlugin
from nospam import utils

log = logging.getLogger(__name__)


class StopWordPlugin(BasePlugin):

    MINWORDLENGTH = 2

    def __init__(self, config):
        dictionary = config.get('stopwords',
                                os.path.join(config.get('config_dir', './conf'),
                                             'stopwords.txt'))
        try:
            self._stopwords = utils.read_file(dictionary)
        except IOError as e:
            log.error('could not read stopwords from %s: %s',
                      dictionary, str(e))
            self._stopwords = []

    def testComment(self, comment):
        '''tests the comment for similarity to stopwords; this is
        "inspired" by the stopwords plugin of blogspam'''
        if not self._stopwords:
            return (0, "OK") # No dictionary, so the comment is surely ok.
        for word in self._splitText(comment['comment']):
            for stopword in self._stopwords:
                if self._aresimilar(word, stopword):
                    return (self.SCORE, "stopwords:%s" % word)
        return (0, "OK")

    def _splitText(self, comment):
        norm = re.compile(r'[\t\n]', re.M | re.U)
        comment = norm.sub(' ', comment)
        sp = re.compile(r'\W+', re.M | re.U)
        return [a.lower() for a in sp.split(comment)
                if len(a) > self.MINWORDLENGTH]

    def _aresimilar(self, word, stopword):
        if word == stopword:
            return True
        if self._levenshtein(word, stopword) <= len(word)/4:
            return True
        #more tests needed
        return None

    def _levenshtein(self, s, t, ins=1, rem=1, subs = 1):
        '''Levenshtein distance, slightly modified off Wikipedia'''
        s = ' ' + s
        t = ' ' + t
        d = {}
        S = len(s)
        T = len(t)
        for i in range(S):
            d[i, 0] = i*ins
        for j in range (T):
            d[0, j] = j*rem
        for j in range(1, T):
            for i in range(1, S):
                if s[i] == t[j]:
                    d[i, j] = d[i-1, j-1]
                else:
                    d[i, j] = min(d[i-1, j] + ins, d[i, j-1] + rem, d[i-1, j-1] + subs)
        return d[S-1, T-1]
