import os
import tempfile
import unittest
from nospam import utils


class UtilsTest(unittest.TestCase):

    def setUp(self):
        self.tmpfiles = []

    def tearDown(self):
        for f in self.tmpfiles:
            os.remove(f)

    def test_read_file(self):
        tmpfd, tmpfile = tempfile.mkstemp()
        os.write(tmpfd, '# a comment\n\n\nline\n'.encode('utf-8'))
        os.close(tmpfd)
        self.tmpfiles.append(tmpfile)

        result = utils.read_file(tmpfile)
        self.assertEquals(['line'], result)


if __name__ == '__main__':
    unittest.main()
