import os
import shutil
import tempfile
import unittest
from nospam.urls import *


class TestFindUrls(unittest.TestCase):

    def test_simple_url(self):
        s = 'http://inventati.org/'
        res = find_urls(s)
        self.assertEquals(1, len(res))
        self.assertEquals('http://inventati.org/', res[0])

    def test_multiple_urls(self):
        s = "I'm undecided between http://autistici.org and http://inventati.org"
        res = find_urls(s)
        self.assertEquals(2, len(res))
        self.assertEquals('http://autistici.org', res[0])
        self.assertEquals('http://inventati.org', res[1])

    def test_some_nasty_urls(self):
        tst = [
            ('w http://inventati.org!', 'http://inventati.org'),
            ('(http://inventati.org)', 'http://inventati.org'),
            ('[http://inventati.org]', 'http://inventati.org'),
            ('click on [http://inventati.org]to win', 'http://inventati.org'),
            ]
        for haystack, needle in tst:
            res = find_urls(haystack)
            self.assertEquals(needle, res[0],
                              "find url failed for %s" % haystack)


class TestDomains(unittest.TestCase):

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        open(os.path.join(self.tmpdir, 'two-level-tlds'), 'w').write(
            'test2.com\n')
        open(os.path.join(self.tmpdir, 'three-level-tlds'), 'w').write(
            'b.test3.com\n')
        open(os.path.join(self.tmpdir, 'domain-whitelist'), 'w').write(
            'noblogs.org\n')
        init_urls({'config_dir': self.tmpdir, 'data_dir': self.tmpdir})

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_is_ipaddr(self):
        self.assertTrue(is_ipaddr('10.0.0.1'))
        self.assertFalse(is_ipaddr('32.noblogs.org'))
        self.assertFalse(is_ipaddr('noblogs.org'))

    def test_strip_subdomain(self):
        self.assertEquals('test.com', strip_subdomain('www.test.com'))
        self.assertEquals('test.com', strip_subdomain('a.b.c.test.com'))

        self.assertEquals('a.test2.com', 
                          strip_subdomain('a.test2.com'))
        self.assertEquals('a.test2.com', 
                          strip_subdomain('1.a.test2.com'))
        self.assertEquals('a.test2.com',
                          strip_subdomain('1.2.a.test2.com'))
        self.assertEquals('test2.com', 
                          strip_subdomain('test2.com'))

        self.assertEquals('a.b.test3.com',
                          strip_subdomain('a.b.test3.com'))
        self.assertEquals('a.b.test3.com',
                          strip_subdomain('www.a.b.test3.com'))
        self.assertEquals('test3.com', strip_subdomain('www.test3.com'))

    def test_get_domains_ignores_empty_urls(self):
        urls = ['#comment', '/about']
        result = list(get_domains(urls))
        self.assertEquals([], result)

    def test_get_domains_strips_subdomains(self):
        urls = ['http://www.test.com/', 'http://www.org.test2.com/']
        result = list(get_domains(urls))
        self.assertEquals(['test.com', 'org.test2.com'], result)

    def test_get_domains_with_ip_addresses(self):
        urls = ['http://10.0.0.1/']
        result = list(get_domains(urls))
        self.assertEquals(['10.0.0.1'], result)

    def test_get_domains_strips_path(self):
        urls = ['http://www.test.com/about']
        result = list(get_domains(urls))
        self.assertEquals(['test.com'], result)

    def test_get_domains_ignores_whitelist(self):
        urls = ['http://www.noblogs.org/']
        result = list(get_domains(urls))
        self.assertEquals([], result)        


if __name__ == '__main__':
    unittest.main()
