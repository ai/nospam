from nospam.plugin_base import BasePlugin


class SamplePlugin(BasePlugin):

    enabled = True

    def testComment(self, comment):
        if comment['comment'] == 'spam':
            return (1.0, 'spam')
        else:
            return (0, 'OK')

