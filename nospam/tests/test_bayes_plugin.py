import os
import shutil
import tempfile
import unittest
from nospam.plugins.bayes import BayesianPlugin


class BayesianPluginTest(unittest.TestCase):

    def setUp(self):
        self.data_dir = tempfile.mkdtemp()
        self.bayes = BayesianPlugin({'data_dir': self.data_dir})

    def tearDown(self):
        shutil.rmtree(self.data_dir)

    def test_works_with_empty_db(self):
        comment = {'comment': 'do not know'}
        score, msg = self.bayes.testComment(comment)
        self.assertEquals(0, score)

    def test_classify(self):
        train_comments = [{'comment': 'this is really spam', 'train': 'spam'},
                          {'comment': 'this should be ok', 'train': 'ok'}]
        test_comments = [{'comment': 'this is really spam', 'train': 'spam'},
                         {'comment': 'ok', 'train': 'ok'}]
        for c in train_comments:
            self.bayes.classifyComment(c)
        for c in test_comments:
            score, msg = self.bayes.testComment(c)
            if score == 0:
                result = 'ok'
            else:
                result = 'spam'
            self.assertEquals(c['train'], result,
                              "'%s' misclassified as %s" % (c['comment'], result))
